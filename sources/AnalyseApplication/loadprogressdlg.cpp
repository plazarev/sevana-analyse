#include "stdafx.h"
#include "loadprogressdlg.h"

LoadProgressDlg::LoadProgressDlg(QWidget *parent)
	: QDialog(parent)
	, m_total (100)
	, m_needClose(false)
{
	ui.setupUi(this);
}

LoadProgressDlg::~LoadProgressDlg()
{

}

void LoadProgressDlg::setRange(int total) {
	m_total = total;
	ui.progressBar->setRange(0, m_total);
}

void LoadProgressDlg::progress(int step, int total) {
	ui.progressBar->setValue(step);
}

void LoadProgressDlg::closeProgress() {
	m_needClose = true;
	close();
}

void LoadProgressDlg::reject() {
	if (!m_needClose) {
		emit loadCancelled();
	}
	else {
		QDialog::reject();
	}
}

void LoadProgressDlg::showEvent(QShowEvent *event) {
	m_needClose = false;
	ui.progressBar->setValue(0);
}
