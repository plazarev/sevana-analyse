#include "stdafx.h"
#include "MediaFilesManager.h"
#include "asyncfilesloader.h"


AsyncFilesLoader::AsyncFilesLoader(QObject *parent, MediaFilesManager* filesManager)
	: QObject(parent)
	, m_pMediaFilesManager(filesManager)
	, m_bNeedStop(false)
{

}

AsyncFilesLoader::~AsyncFilesLoader()
{
	int a = 0;
	a++;
}

void AsyncFilesLoader::setFilesList(const QStringList& filesPathList) {
	m_filesPathList = filesPathList;
}

void AsyncFilesLoader::cancel() {
	setNeedStop(true);
}

void AsyncFilesLoader::run() {
	setNeedStop(false);

	QStringList loadedFiles;

	bool bStopped = false;
	int step = 0;
	int total = m_filesPathList.count();

	for (QStringList::const_iterator it = m_filesPathList.cbegin(); it != m_filesPathList.cend(); it++) {
		{
			QMutexLocker locker(&m_mutex);
			if (m_bNeedStop) {
				bStopped = true;
				break;
			}
		}
		step++;

		QString fileName = *it;

		m_pMediaFilesManager->addFile(fileName);

		emit fileAdded(fileName);
		emit progress(step, total);

		loadedFiles.append(fileName);
	}

	if (bStopped) {
		for (QStringList::const_iterator it = loadedFiles.cbegin(); it != loadedFiles.cend(); it++) {
			step--;
			QString fileName = *it;

			m_pMediaFilesManager->removeFile(fileName);

			emit fileCancelled(fileName);
			emit progress(step, total);
		}
	}

	emit finished();
}

bool AsyncFilesLoader::isNeedStop() {
	QMutexLocker locker(&m_mutex);
	return m_bNeedStop;
}

void AsyncFilesLoader::setNeedStop(bool isNeed) {
	QMutexLocker locker(&m_mutex);
	m_bNeedStop = isNeed;
}
