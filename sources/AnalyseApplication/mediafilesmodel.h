#ifndef MEDIAFILESMODEL_H
#define MEDIAFILESMODEL_H

#include <QAbstractTableModel>

class MediaFilesManager;
class MediaFileInformation;
class MediaCharacteristic;


class MediaFilesModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	MediaFilesModel(QObject *parent, MediaFilesManager* pFilesManager);
	~MediaFilesModel();

	void refresh();

	int rowsCount() const;
	int columnsCount() const;

	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

private:
	MediaFileInformation* getColumnInformation(int column) const;
	MediaCharacteristic*  getRowInformation(MediaFileInformation* pFileInfo, int row) const;

private:
	MediaFilesManager* m_pFilesManager;
};

#endif // MEDIAFILESMODEL_H
