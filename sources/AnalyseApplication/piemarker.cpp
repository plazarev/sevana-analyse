#include "stdafx.h"
#include "PieMarker.h"

#include "qwt_plot_curve.h"
#include "PiePlot.h"


/*!
* \brief PieMarker::PieMarker constructor of PieMarker class
*/
PieMarker::PieMarker(int width) {
	setZ(1000);

	setRenderHint(QwtPlotItem::RenderAntialiased, true);

	this->height = width;
	this->width = width;
	this->margin = 8;
}

PieMarker::~PieMarker() {

}

int PieMarker::rtti() const
{
	return QwtPlotItem::Rtti_PlotUserItem;
}

void PieMarker::draw(QPainter *p, const QwtScaleMap &, const QwtScaleMap &, const QRectF &rect) const {
	const PiePlot *piePlot = (PiePlot *)plot();
	const QwtScaleMap yMap = piePlot->canvasMap(QwtPlot::yLeft);

	QRect pieRect;
	pieRect.setX(rect.x() + rect.width()/2 - width/2);
	pieRect.setY(rect.y() + rect.height() / 2 - height/2);
	pieRect.setHeight(height);
	pieRect.setWidth(width);


	int angle = (int)(5760 * 0.75);
	const QwtPlotCurve* pCurve = piePlot->getPieValueCurve();
	if (pCurve->dataSize() > 0)
	{
		int value = (int)(5760 * pCurve->sample(0).y() / 100.0);

		p->save();

		p->setBrush(QBrush(pCurve->pen().color(), Qt::SolidPattern));
		if (value != 0) {
			p->drawPie(pieRect, -angle, -value);
		}
		angle += value;

		value = (int)(5760 * (100 - pCurve->sample(0).y()) / 100.0);
		p->setBrush(QBrush(Qt::lightGray, Qt::SolidPattern));
		p->setPen(Qt::lightGray);
		if (value != 0) {
			p->drawPie(pieRect, -angle, -value);
		}

		angle += value;


		p->restore();
	}

	/*
	const QwtPlotCurve* pBackCurve = piePlot->getPieBackCurve();
	if (pBackCurve->dataSize() > 0)
	{
		const int value = (int)(5760 * pBackCurve->sample(0).y() / 100.0);

		p->save();
		p->setBrush(QBrush(pBackCurve->pen().color(), Qt::SolidPattern));
		p->setPen(Qt::lightGray);
		if (value != 0)
			p->drawPie(pieRect, -angle, -value);
		p->restore();

		angle += value;
	}
	*/
}