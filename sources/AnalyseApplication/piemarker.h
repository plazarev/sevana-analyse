#ifndef PIEMARKER_H
#define PIEMARKER_H

#include <qwt_plot_item.h>

class PieMarker : public QwtPlotItem
{
public:
	PieMarker(int width);
	~PieMarker();

	virtual int rtti() const;

	virtual void draw(QPainter *p, const QwtScaleMap &, const QwtScaleMap &, const QRectF &rect) const;

private:
	int height;
	int width;
	int margin;

};

#endif // PIEMARKER_H
