#ifndef LOADPROGRESSDLG_H
#define LOADPROGRESSDLG_H

#include <QDialog>
#include "ui_loadprogressdlg.h"

class LoadProgressDlg : public QDialog
{
	Q_OBJECT

public:
	LoadProgressDlg(QWidget *parent = 0);
	~LoadProgressDlg();

	void setRange(int total);

public slots:
	void progress(int step, int total);
	void closeProgress();
	virtual void reject();

signals:
	void loadCancelled();

protected:
	virtual void showEvent(QShowEvent *event);

private:
	Ui::LoadProgressDlg ui;
	int m_total;
	bool m_needClose;
};

#endif // LOADPROGRESSDLG_H
