#ifndef __MEDIA_FILES_MANAGER__H__
#define __MEDIA_FILES_MANAGER__H__

#include <QObject>
#include <QDateTime>


class MediaFileInformation;

class MediaFilesManager : public QObject
{
	Q_OBJECT

public:
	MediaFilesManager(QObject* parent);
	~MediaFilesManager();

	void addFile(const QString& fileName);
	void removeFile(const QString& fileName);
	void removeAll();

	const QMap<QDateTime, MediaFileInformation*>& getFiles() const;

private:
	QMap<QDateTime, MediaFileInformation*> m_files;
};

#endif //__MEDIA_FILES_MANAGER__H__