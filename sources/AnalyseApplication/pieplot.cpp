#include "stdafx.h"
#include "pieplot.h"
#include "qwt_plot_item.h"
#include "qwt_plot_curve.h"
#include "qwt_plot_canvas.h"
#include "qwt_plot_layout.h"
#include "qwt_legend.h"

#include "piemarker.h"

/*!
* \brief The PiePlotCurve class that creates a curve.
*/
class PiePlotCurve : public QwtPlotCurve
{
public:
	PiePlotCurve(const QString &title) :
		QwtPlotCurve(title)
	{
		setRenderHint(QwtPlotItem::RenderAntialiased);
	}

	void setColor(const QColor &color)
	{
		QColor c = color;
		c.setAlpha(150);

		setPen(c);
		setBrush(c);
	}

	double* getValue() {
		return value;
	}

	void setValue(double val) {
		value[0] = val;
	}

private:
	double value[1];
};


PiePlot::PiePlot(const QString& pieName, int pieValue, int backValue, const QColor& pieColor, int width, QWidget *parent)
	: QwtPlot(parent)
{
	QwtPlotCanvas* plotCanvas = qobject_cast<QwtPlotCanvas*>(canvas());
	plotCanvas->setBorderRadius(0);
	plotCanvas->setContentsMargins(0, 0, 0, 10);
	plotCanvas->setFrameStyle(QFrame::NoFrame);

	plotLayout()->setAlignCanvasToScales(true);

	QwtLegend* pLegend = new QwtLegend();
	insertLegend(pLegend, QwtPlot::BottomLegend);

	enableAxis(QwtPlot::yLeft, false);
	enableAxis(QwtPlot::xBottom, false);

	setTitle(pieName);

	PieMarker* pie = new PieMarker(width);
	pie->attach(this);

	m_time[0] = 0;

	m_pValueCurve = new PiePlotCurve(QString("%1%").arg(pieValue));
	m_pValueCurve->setColor(pieColor);
	m_pValueCurve->setValue(pieValue);
	m_pValueCurve->attach(this);
	m_pValueCurve->setRawSamples(m_time, m_pValueCurve->getValue(), 1);
	m_pValueCurve->setVisible(false);

	/*
	m_pBackCurve = new PiePlotCurve("");
	m_pBackCurve->setColor(Qt::lightGray);
	m_pBackCurve->setValue(backValue);
	m_pBackCurve->attach(this);
	m_pBackCurve->setRawSamples(m_time, m_pBackCurve->getValue(), 1);
	m_pBackCurve->setVisible(false);
	m_pBackCurve->setLegendAttribute(QwtPlotCurve::LegendNoAttribute, false);
	*/

	replot();
}

PiePlot::~PiePlot()
{

}

const QwtPlotCurve* PiePlot::getPieValueCurve() const {
	return m_pValueCurve;
}

const QwtPlotCurve* PiePlot::getPieBackCurve() const {
	return m_pBackCurve;
}
