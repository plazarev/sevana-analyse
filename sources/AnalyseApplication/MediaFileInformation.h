#ifndef __MEDIA_FILE_INFORMATION__H__
#define __MEDIA_FILE_INDORMATION__H__

#include <QDateTime>

class MediaCharacteristic;

class MediaFileInformation
{
public:
	MediaFileInformation(const QString& filePath);
	MediaFileInformation(const MediaFileInformation& mfi);
	~MediaFileInformation();

	const QString& getPath() const;
	const QString& getName() const;

	const QDateTime& getFileDate() const;

	const QMap<QString, MediaCharacteristic*>& getCharacteristics() const;
	void addCharacteristic(MediaCharacteristic* characteristic);

private:
	QString m_path;
	QString m_name;
	QDateTime m_date;

	QMap<QString, MediaCharacteristic*> m_characteristics;
};

#endif //__MEDIA_FILE_INDORMATION__H__
