#include "stdafx.h"
#include "MediaFileInformation.h"
#include "MediaCharacteristic.h"

MediaFileInformation::MediaFileInformation(const QString& filePath) {
	m_path = filePath;

	QFileInfo fileInfo(m_path);
	m_name = fileInfo.fileName();
	m_date = fileInfo.lastModified();
}

MediaFileInformation::MediaFileInformation(const MediaFileInformation& mfi) {
	m_path = mfi.getPath();
	m_name = mfi.getName();
	m_date = mfi.getFileDate();

	const QMap<QString, MediaCharacteristic*>& chMap = mfi.getCharacteristics();
	for (QMap<QString, MediaCharacteristic*>::const_iterator it = chMap.cbegin(); it != chMap.cend(); it++) {
		addCharacteristic(*it);
	}
}

MediaFileInformation::~MediaFileInformation() {
	for (QMap<QString, MediaCharacteristic*>::const_iterator it = m_characteristics.cbegin(); it != m_characteristics.cend(); it++) {
		delete *it;
	}
}

const QString& MediaFileInformation::getPath() const {
	return m_path;
}

const QString& MediaFileInformation::getName() const {
	return m_name;
}

const QDateTime& MediaFileInformation::getFileDate() const {
	return m_date;
}

const QMap<QString, MediaCharacteristic*>& MediaFileInformation::getCharacteristics() const {
	return m_characteristics;
}

void MediaFileInformation::addCharacteristic(MediaCharacteristic* characteristic) {
	m_characteristics.insert(characteristic->getName(), characteristic);
}
