#include "stdafx.h"
#include "MediaFilesManager.h"
#include "MediaFileInformation.h"
#include "MediaCharacteristic.h"


MediaFilesManager::MediaFilesManager(QObject* parent)
	: QObject(parent)
{
}


MediaFilesManager::~MediaFilesManager()
{
}

void MediaFilesManager::addFile(const QString& fileName) {
	MediaFileInformation* pInfo = new MediaFileInformation(fileName);

	QStringList characteristicNames;
	characteristicNames << "MOS" << "Quality" << "Jitter";

	for (QStringList::const_iterator it = characteristicNames.cbegin(); it != characteristicNames.cend(); it++) {
		MediaCharacteristic* pCharacteristic = new MediaCharacteristic(*it, (qrand() * 100) / RAND_MAX );
		pInfo->addCharacteristic(pCharacteristic);
	}


	m_files.insert(pInfo->getFileDate(), pInfo);
}

void MediaFilesManager::removeFile(const QString& fileName) {
	for (QMap<QDateTime, MediaFileInformation*>::iterator it = m_files.begin(); it != m_files.end(); it++) {

		MediaFileInformation* pInfo = *it;
		if (pInfo->getPath().compare(fileName) == 0) {
			m_files.erase(it);
			delete pInfo;

			return;
		}
	}
}

void MediaFilesManager::removeAll() {
	for (QMap<QDateTime, MediaFileInformation*>::iterator it = m_files.begin(); it != m_files.end(); it++) {
		delete *it;
	}
	m_files.clear();
}

const QMap<QDateTime, MediaFileInformation*>& MediaFilesManager::getFiles() const {
	return m_files;
}