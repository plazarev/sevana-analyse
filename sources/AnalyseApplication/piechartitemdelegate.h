#ifndef PIECHARTITEMDELEGATE_H
#define PIECHARTITEMDELEGATE_H

#include <QStyledItemDelegate>

class PieChartItemDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	PieChartItemDelegate(QObject *parent);
	~PieChartItemDelegate();

private:
	virtual void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const;
	virtual QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const;

};

#endif // PIECHARTITEMDELEGATE_H
