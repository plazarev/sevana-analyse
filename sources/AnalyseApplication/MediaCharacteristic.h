#ifndef __MEDIA_CHARACTERISTIC__H__
#define __MEDIA_CHARACTERISTIC__H__

class MediaCharacteristic
{
public:
	MediaCharacteristic();
	MediaCharacteristic(const QString& name, const QVariant& value);
	MediaCharacteristic(const MediaCharacteristic& mc);
	~MediaCharacteristic();

	const QString& getName() const;
	const QVariant& getValue() const;

private:
	QString  m_name;
	QVariant m_value;
};


Q_DECLARE_METATYPE(MediaCharacteristic)

#endif //__MEDIA_CHARACTERISTIC__H__