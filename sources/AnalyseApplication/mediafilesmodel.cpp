#include "stdafx.h"
#include "mediafilesmodel.h"
#include "MediaFilesManager.h"
#include "MediaFileInformation.h"
#include "MediaCharacteristic.h"


MediaFilesModel::MediaFilesModel(QObject *parent, MediaFilesManager* pFilesManager)
	: QAbstractTableModel(parent)
	, m_pFilesManager(pFilesManager)
{

}

MediaFilesModel::~MediaFilesModel()
{

}

void MediaFilesModel::refresh() {
	beginResetModel();
	endResetModel();
}

int MediaFilesModel::rowsCount() const {
	const QMap<QDateTime, MediaFileInformation*>& files = m_pFilesManager->getFiles();

	if (!files.isEmpty()) {
		MediaFileInformation* pInfo = files.first();
		return pInfo->getCharacteristics().count();
	}

	return 0;
}

int MediaFilesModel::columnsCount() const {
	return m_pFilesManager->getFiles().count();
}

int MediaFilesModel::rowCount(const QModelIndex & /*parent*/) const {
	return rowsCount();
}

int MediaFilesModel::columnCount(const QModelIndex & /*parent*/) const {
	return columnsCount();
}

QVariant MediaFilesModel::data(const QModelIndex &index, int role) const {
	int column = index.column();
	int row = index.row();

	do 
	{
		if (role == Qt::DisplayRole) {
			MediaFileInformation* pInfo = getColumnInformation(column);
			if (NULL == pInfo) {
				break;
			}

			MediaCharacteristic* pCh = getRowInformation(pInfo, row);
			if (NULL == pCh) {
				break;
			}

			int percent = pCh->getValue().toInt();

			return QString("%1%").arg(percent);
		}
		else if (role == Qt::UserRole) {
			MediaFileInformation* pInfo = getColumnInformation(column);
			if (NULL == pInfo) {
				break;
			}

			MediaCharacteristic* pCh = getRowInformation(pInfo, row);
			if (NULL == pCh) {
				break;
			}

			return QVariant::fromValue(*pCh);
		}
	} while (false);

	return QVariant();
}

QVariant MediaFilesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal) {
			MediaFileInformation* pInfo = getColumnInformation(section);
			return pInfo->getFileDate().toString("d MMM yy");
		}
		else
		{
			MediaFileInformation* pInfo = getColumnInformation(0);
			MediaCharacteristic* pCh = getRowInformation(pInfo, section);
			return pCh->getName();
		}
	}
	return QVariant();
}

MediaFileInformation* MediaFilesModel::getColumnInformation(int column) const {
	const QMap<QDateTime, MediaFileInformation*>& files = m_pFilesManager->getFiles();
	QList<MediaFileInformation*> infoList = files.values();
	if (!infoList.isEmpty()) {
		return infoList.at(column);
	}
	return NULL;
}

MediaCharacteristic*  MediaFilesModel::getRowInformation(MediaFileInformation* pFileInfo, int row) const {
	const QMap<QString, MediaCharacteristic*>& characheristicksMap = pFileInfo->getCharacteristics();
	QList<MediaCharacteristic*> chList = characheristicksMap.values();
	if (!chList.isEmpty()) {
		return chList.at(row);
	}
	return NULL;
}
