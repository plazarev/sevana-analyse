#ifndef ANALYSEAPPLICATION_H
#define ANALYSEAPPLICATION_H

#include <QtWidgets/QMainWindow>
#include "ui_analyseapplication.h"

#include "ComparablePoint.h"

class MediaFilesManager;
class LoadProgressDlg;
class AsyncFilesLoader;
class MediaFilesModel;
class PieChartItemDelegate;
class PiePlot;

class AnalyseApplication : public QMainWindow
{
	Q_OBJECT

public:
	AnalyseApplication(QWidget *parent = 0);
	~AnalyseApplication();

private slots:
	void onFileAdd();
	void onExit();
	void onAbout();
	void onAsyncFileAdded(const QString& filePath);
	void onAsyncFileRemoved(const QString& filePath);
	void onAsyncLoadRejected();
	void onAsyncLoadFinished();

signals:
	void onFileAdded(const QString& fileName);

protected:
	virtual void closeEvent(QCloseEvent * event);
	void addFilesAsync(const QStringList& fileNames);

	void updateChartsView();
	void cleanupCharts();

private:
	Ui::AnalyseApplicationClass ui;
	MediaFilesManager* m_pFilesManager;
	LoadProgressDlg* m_progressDlg;
	AsyncFilesLoader* m_fileLoader;
	MediaFilesModel* m_filesModel;
	PieChartItemDelegate* m_pieChartItemDelegate;

	QMap<ComparablePoint, PiePlot*> m_charts;
};

#endif // ANALYSEAPPLICATION_H
