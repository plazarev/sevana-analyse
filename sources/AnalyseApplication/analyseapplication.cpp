#include "stdafx.h"

#include <QFileDialog>
#include <QStringList>
#include <QThreadPool>
#include <QAbstractItemModel>

#include "analyseapplication.h"
#include "aboutdialog.h"
#include "loadprogressdlg.h"
#include "asyncfilesloader.h"
#include "mediafilesmodel.h"
#include "piechartitemdelegate.h"
#include "MediaFilesManager.h"
#include "pieplot.h"
#include "MediaCharacteristic.h"

AnalyseApplication::AnalyseApplication(QWidget *parent)
	: QMainWindow(parent)
	, m_pFilesManager(NULL)
{
	ui.setupUi(this);
	m_pFilesManager = new MediaFilesManager(this);
	m_progressDlg = new LoadProgressDlg(this);
	m_fileLoader = new AsyncFilesLoader(this, m_pFilesManager);
	m_filesModel = new MediaFilesModel(this, m_pFilesManager);
	m_pieChartItemDelegate = new PieChartItemDelegate(ui.chartsTableView);

	QObject::connect(m_fileLoader, SIGNAL(fileAdded(QString)), this, SLOT(onAsyncFileAdded(QString)));
	QObject::connect(m_fileLoader, SIGNAL(fileCancelled(QString)), this, SLOT(onAsyncFileRemoved(QString)));
	QObject::connect(m_fileLoader, SIGNAL(progress(int, int)), m_progressDlg, SLOT(progress(int, int)));
	QObject::connect(m_fileLoader, SIGNAL(finished()), this, SLOT(onAsyncLoadFinished()));

	QObject::connect(m_progressDlg, SIGNAL(loadCancelled()), this, SLOT(onAsyncLoadRejected()));

	ui.chartsTableView->setModel(m_filesModel);
	ui.chartsTableView->setItemDelegate(m_pieChartItemDelegate);

	ui.dataTableView->setModel(m_filesModel);
}

AnalyseApplication::~AnalyseApplication()
{
	m_pFilesManager->removeAll();
	delete m_progressDlg;
	delete m_fileLoader;

	cleanupCharts();
}

void AnalyseApplication::onFileAdd() {
	QStringList fileNames = QFileDialog::getOpenFileNames(
		this,
		tr("Select one or more files to open"),
		"",
		"Voice files (*.*)");

	addFilesAsync(fileNames);
}

void AnalyseApplication::onExit() {
	close();
}

void AnalyseApplication::onAbout() {
	AboutDialog dlg;
	dlg.exec();
}

void AnalyseApplication::onAsyncFileAdded(const QString& filePath) {
	ui.filesListWidget->addItem(filePath);
}

void AnalyseApplication::onAsyncFileRemoved(const QString& filePath) {
	QList<QListWidgetItem*> items = ui.filesListWidget->findItems(filePath, Qt::MatchExactly);
	for (QList<QListWidgetItem*>::const_iterator it = items.cbegin(); it != items.cend(); it++) {
		delete ui.filesListWidget->takeItem(ui.filesListWidget->row(*it));
	}
}

void AnalyseApplication::onAsyncLoadRejected() {
	m_fileLoader->cancel();
}

void AnalyseApplication::onAsyncLoadFinished() {
	m_progressDlg->closeProgress();
	m_filesModel->refresh();

	updateChartsView();
}

void AnalyseApplication::closeEvent(QCloseEvent * event) {
	m_pFilesManager->removeAll();
	event->accept();
}

void AnalyseApplication::addFilesAsync(const QStringList& fileNames) {
	m_fileLoader->setFilesList(fileNames);
	m_fileLoader->setAutoDelete(false);
	m_progressDlg->setRange(fileNames.count());

	QThreadPool::globalInstance()->start(m_fileLoader);

	m_progressDlg->exec();
}

void AnalyseApplication::updateChartsView() {
	ui.chartsTableView->resizeColumnsToContents();
	ui.chartsTableView->resizeRowsToContents();

	int width = ui.chartsTableView->columnWidth(0);

	cleanupCharts();

	QAbstractItemModel* pModel = ui.chartsTableView->model();
	for (int col = 0; col < pModel->columnCount(); col++) {
		for (int row = 0; row < pModel->rowCount(); row++) {

			QModelIndex idx = pModel->index(row, col);
			QVariant varValue = pModel->data(idx, Qt::UserRole);
			if (varValue.canConvert<MediaCharacteristic>()) {
				MediaCharacteristic ch = varValue.value<MediaCharacteristic>();

				PiePlot* pPlot = new PiePlot(ch.getName(), ch.getValue().toInt(), 100 - ch.getValue().toInt(), Qt::darkGreen, width*0.7, this);
				m_charts.insert(ComparablePoint(col, row), pPlot);

				ui.chartsTableView->setIndexWidget(idx, pPlot);
			}
		}
	}
}

void AnalyseApplication::cleanupCharts() {
	for (QMap<ComparablePoint, PiePlot*>::const_iterator it = m_charts.cbegin(); it != m_charts.cend(); it++) {
		delete *it;
	}
	m_charts.clear();
}