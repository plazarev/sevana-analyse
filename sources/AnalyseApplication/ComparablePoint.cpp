#include "stdafx.h"
#include "ComparablePoint.h"


ComparablePoint::ComparablePoint() {
}

ComparablePoint::ComparablePoint(int xpos, int ypos)
: QPoint(xpos, ypos)
{
}

ComparablePoint::ComparablePoint(const ComparablePoint& p) 
	: QPoint(p.x(), p.y())
{
}

ComparablePoint::~ComparablePoint() {
}

ComparablePoint& ComparablePoint::operator = (const ComparablePoint& p) {
	if (this != &p) {
		setX(p.x());
		setY(p.y());
	}
	return *this;
}
