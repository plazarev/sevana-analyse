#ifndef __COMPARABLE_POINT__H__
#define __COMPARABLE_POINT__H__

#include <QPoint>

class ComparablePoint : public QPoint
{
public:
	ComparablePoint();
	ComparablePoint(int xpos, int ypos);
	ComparablePoint(const ComparablePoint& p);
	~ComparablePoint();

	ComparablePoint& operator = (const ComparablePoint& p);

	friend inline bool operator < (const ComparablePoint &, const ComparablePoint &);
};

inline bool operator < (const ComparablePoint& p1, const ComparablePoint& p2) {
	return (p1.y() < p2.y()) || (p1.y() == p2.y() && p1.x() < p2.x());
}

#endif //__COMPARABLE_POINT__H__