#ifndef PIEPLOT_H
#define PIEPLOT_H

#include <qwt_plot.h>

class QwtPlotCurve;
class PiePlotCurve;


class PiePlot : public QwtPlot
{
	Q_OBJECT

public:
	PiePlot(const QString& pieName, int pieValue, int backValue, const QColor& pieColor, int width, QWidget *parent);
	~PiePlot();

	const QwtPlotCurve* getPieValueCurve() const;
	const QwtPlotCurve* getPieBackCurve() const;


private:
	PiePlotCurve* m_pValueCurve;
	PiePlotCurve* m_pBackCurve;

	double m_time[1];
};

#endif // PIEPLOT_H
