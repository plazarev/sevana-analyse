#include "stdafx.h"
#include "MediaCharacteristic.h"

MediaCharacteristic::MediaCharacteristic() {

}

MediaCharacteristic::MediaCharacteristic(const QString& name, const QVariant& value) {
	m_name = name;
	m_value = value;
}

MediaCharacteristic::MediaCharacteristic(const MediaCharacteristic& mc) {
	m_name = mc.getName();
	m_value = mc.getValue();
}

MediaCharacteristic::~MediaCharacteristic() {

}

const QString& MediaCharacteristic::getName() const {
	return m_name;
}

const QVariant& MediaCharacteristic::getValue() const {
	return m_value;
}
