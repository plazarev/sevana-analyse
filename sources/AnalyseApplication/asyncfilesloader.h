#ifndef ASYNCFILESLOADER_H
#define ASYNCFILESLOADER_H

#include <QObject>
#include <QMutex>

class MediaFilesManager;

class AsyncFilesLoader : public QObject, 
						 public QRunnable
{
	Q_OBJECT

public:
	AsyncFilesLoader(QObject *parent, MediaFilesManager* filesManager);
	~AsyncFilesLoader();

	void setFilesList(const QStringList& filesPathList);

signals:
	void progress(int step, int total);
	void fileAdded(const QString& filePath);
	void fileCancelled(const QString& filePath);
	void finished();

public slots:
	void cancel();

private:
	virtual void run();

	bool isNeedStop();
	void setNeedStop(bool isNeed);

private:
	QMutex m_mutex;
	QStringList m_filesPathList;
	MediaFilesManager* m_pMediaFilesManager;
	bool m_bNeedStop;
};

#endif // ASYNCFILESLOADER_H
