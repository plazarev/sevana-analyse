#include "stdafx.h"
#include "piechartitemdelegate.h"

PieChartItemDelegate::PieChartItemDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{

}

PieChartItemDelegate::~PieChartItemDelegate()
{

}

void PieChartItemDelegate::paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const {
	//QStyledItemDelegate::paint(painter, option, index);
}

QSize PieChartItemDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const {
	return QSize(400, 400);
	//return QStyledItemDelegate::sizeHint(option, index);
}
